#!/bin/bash
# tested in debian 10 
#vars 
COMP_NAME=krew
declare packagelist1=(
  ctx
  ns
  fuzzy
  ktop
  get-all
  status
  pod-dive
  janitor
  cost
  doctor
  flame
  fleet
  graph
  konfig
)

function InstallKrewAddons() {
for i in ${!packagelist1[@]}; do 
    kubectl krew install ${packagelist1[i]}
done
}

# install krew
(
  set -x; cd "$(mktemp -d)" &&
  OS="$(uname | tr '[:upper:]' '[:lower:]')" &&
  ARCH="$(uname -m | sed -e 's/x86_64/amd64/' -e 's/\(arm\)\(64\)\?.*/\1\2/' -e 's/aarch64$/arm64/')" &&
  KREW="krew-${OS}_${ARCH}" &&
  curl -fsSLO "https://github.com/kubernetes-sigs/krew/releases/latest/download/${KREW}.tar.gz" &&
  tar zxvf "${KREW}.tar.gz" &&
  ./"${KREW}" install krew
)
# update and install addons
export PATH="${KREW_ROOT:-$HOME/.krew}/bin:$PATH"
kubectl krew update
InstallKrewAddons
