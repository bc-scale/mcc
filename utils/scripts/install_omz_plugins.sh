#!/bin/bash
 # vars
COMP_NAME=zsh_plugins
cd ~/.oh-my-zsh/custom/plugins
# fzf
git clone https://github.com/unixorn/fzf-zsh-plugin.git fzf-zsh-plugin
# git-extra-commands
git clone https://github.com/unixorn/git-extra-commands.git git-extra-commands