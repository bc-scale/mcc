#!/bin/bash

unset  HashicorpPluginsToInstall PluginsToInstall SoftVersions
# vars
COMP_NAME=asdf
declare -a HashicorpPluginsToInstall=(boundary consul nomad packer sentinel serf terraform vault waypoint)
declare -a PluginsToInstall=(
    kubectl 
    tmux 
    awscli
    direnv
    yarn
    "asdf-alias https://github.com/andrewthauer/asdf-alias.git" 
    "argo https://github.com/sudermanjr/asdf-argo.git" 
    "rust https://github.com/asdf-community/asdf-rust.git" 
    "1password-cli https://github.com/NeoHsu/asdf-1password-cli.git" 
    "eksctl https://github.com/elementalvoid/asdf-eksctl.git"
    "kustomize https://github.com/Banno/asdf-kustomize.git"
    "k9s https://github.com/looztra/asdf-k9s"
    "docker-compose https://github.com/virtualstaticvoid/asdf-docker-compose.git"
    "tmux https://github.com/aphecetche/asdf-tmux.git"
    "krew https://github.com/nlamirault/asdf-krew.git"
    "helm https://github.com/Antiarchitect/asdf-helm.git"
    "kubetail https://github.com/janpieper/asdf-kubetail.git"
    "tflint https://github.com/skyzyx/asdf-tflint"
    "nodejs https://github.com/asdf-vm/asdf-nodejs.git"
    "pulumi https://github.com/canha/asdf-pulumi.git"
    "kind https://github.com/reegnz/asdf-kind.git"
    "golang https://github.com/kennyp/asdf-golang.git"
    "hugo https://github.com/beardix/asdf-hugo.git"
)

declare -a SoftVersions=(
    #"golang 1.17.5"
    #"golang 1.16.10"
    #"eksctl latest"
    #"kustomize latest"
    "k9s latest"
    #"krew latest"
    #"direnv latest"
    #"helm latest"
    "kubetail latest"
    #"docker-compose latest"
    #"kubectl latest"
    #"kubectl 1.15.0"
    #"kubectl 1.17.0"
    #"helm latest "
    #"packer latest"
    #"terraform latest"
    #"terraform 0.11.11"
    #"terraform 0.12.21"
    #"terraform 0.13.6"
    #"terraform 0.14.7"
    #"terraform 0.15.1"
    #"terraform 1.0.0"
    #"vault latest"
    #"vault 1.3.2"
    #"nodejs latest"
    #"nodejs 12.22.0"
    #"tflint latest"
    #"tflint 0.23.1"
    #"golang latest" 
    #"golang 1.17.1"
    #"golang 1.17.5"
    #"golang 1.16.10"
    #"pulumi latest"
    #"tmux latest"
    #"awscli latest"
    #"kind latest"
    #"kind 0.11.1"
    #"hugo latest"
    #"yarn latest"
)

# source ~/zshrc
source ~/zshrc

# adsf 
. ~/.asdf/asdf.sh
fpath=(${ASDF_DIR}/completions $fpath)
autoload -Uz compinit && compinit

# asdf hashicorp plugins installs
for i in ${!HashicorpPluginsToInstall[@]}; do 
    asdf plugin-add ${HashicorpPluginsToInstall[i]} https://github.com/asdf-community/asdf-hashicorp.git
done
# asdf plugins installs
for z in ${!PluginsToInstall[@]}; do 
    asdf plugin-add ${PluginsToInstall[z]}
done
# Asdf software version installs
for x in ${!SoftVersions[@]}; do 
    asdf install ${SoftVersions[x]}
done
